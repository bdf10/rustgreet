use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short = "n", long = "name", default_value = "world")]
    name: String,
}

fn main() {
    let args = Cli::from_args();
    println!("hello {}!", args.name);
}
